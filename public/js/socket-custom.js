var socket = io();

socket.on("connect", function () {
    console.log("Conectado al servidor");
});

socket.on("disconnect" , function () {
    console.log("Perdimos conexion con el servidor");
});

//socket.emit ('enviarMensaje',{usuario: 'Juan mancor' , mensaje: 'Hola socket.io'});
socket.emit(
    "enviarMensaje",
    {
        usuario: "Juan",
        mensaje: "Hola socket io",
    }
);

socket.on("enviarMensaje", function(mensaje){
    console.log("Servidor mensaje: ", mensaje);
});